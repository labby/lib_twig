<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module			Twig Template Engine
 * @author			LEPTON Project
 * @copyright		2012-2019 LEPTON  
 * @link			https://www.LEPTON-cms.org
 * @license			http://www.gnu.org/licenses/gpl.html
 * @license_terms	please see info.php of this module
 *
 */

class lib_twig extends LEPTON_abstract
{
    public static $instance;
    
    public function initialize()
    {
    
    }
    
	/**
	 * Registers Twig_Autoloader as an SPL autoloader.
	 *
	 * @param bool $prepend Whether to prepend the autoloader or not.
	 */
	public static function register($prepend = true)
	{
		spl_autoload_register(array(__CLASS__, 'autoload'), true, $prepend);
	}

	/**
	 * Handles autoloading of classes.
	 *
	 * @param string $class A class name.
	 */
	public static function autoload($class)
	{
	    if (0 !== strpos($class, 'Twig'))
	    {
	        return;
	    }
		// any namespaces?
		$aTempTerms = explode("\\", $class);
		if(1 < count($aTempTerms) )
		{
            array_shift( $aTempTerms ); // remove the \Twig\
            $sPath = implode( DIRECTORY_SEPARATOR, $aTempTerms);
            $file = dirname(dirname(__FILE__)).'/src/'.$sPath.".php";
            
            if (is_file($file))
            {
                require_once $file;
            }
        } else {
            // old way? old way!
            
            $sPath = str_replace("_", DIRECTORY_SEPARATOR, $class);
            
            $file = dirname(dirname(__FILE__)).'/'.$sPath.".php";
            
            if (is_file($file))
            {
                require_once $file;
                return true;
            }
 
            return;
        }
	}
}
?>