<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          Twig Template Engine
 * @author          LEPTON Project
 * @copyright       2012-2019 LEPTON  
 * @link            https://www.LEPTON-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

class lib_twig_operators extends \Twig\Extension\AbstractExtension
{
    // initialize
    public function __construct()
    {
    
    }
    
    /**
     *  See: page 40 ff. inside the twig documentation-pdf. 
     *      https://twig.symfony.com/doc/2.x/
     *      https://twig.symfony.com/doc/2.x/advanced.html#operators
     */
    public function getOperators()
    {
        return array(
            array(
                '!' => array(
                    'precedence' => 50,
                    'class' => 'Twig\Node\Expression\Unary\NotUnary'
                ),
                
                '¬' => array(
                    'precedence' => 50,
                    'class' => 'Twig\Node\Expression\Unary\NotUnary'
                )
             ),
            array(
                '||' => array(
                    'precedence' => 10,
                    'class' => 'Twig\Node\Expression\Binary\OrBinary',
                    'associativity' => \Twig\ExpressionParser::OPERATOR_LEFT
                ),
                '&&' => array(
                    'precedence' => 15,
                    'class' => 'Twig\Node\Expression\Binary\AndBinary',
                    'associativity' => \Twig\ExpressionParser::OPERATOR_LEFT
                )
            )
        );
    }
}