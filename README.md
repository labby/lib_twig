### lib_twig
============

TWIG Template engine as a library for LEPTON-CMS

#### Requirements

* [LEPTON CMS][1]

#### Installation

* download latest [lib_twig.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

The 1-series of TWIG has reached end of life.
Starting with LEPTON-CMS 3.x we use the TWIG 2series within the LEPTON package
Starting with LEPTON-CMS 4.5 we use the TWIG 3series within the LEPTON package

Find more information how to use twig on [Twig Homepage][3]


[1]: https://lepton-cms.org
[2]: http://www.lepton-cms.com/lepador/libraries/lib_twig.php
[3]: https://twig.symfony.com/

